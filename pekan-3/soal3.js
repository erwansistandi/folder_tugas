// 3. Spread Operator
// Terdapat dua buah array seperti berikut:

const arr1 = [10, 15, 20, 30, 50];

const arr2 = [5, 2, 8, 15, 17];

// Gabungkanlah kedua value array tersebut menggunakan Spread Operator,

const spreadOperator = [...arr1, ...arr2];
console.log(spreadOperator);