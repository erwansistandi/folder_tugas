// 9. Menjalankan Promise
// Terdapat sebuah function promise seperti berikut:

const fetchData = (isSuccess) => {

  return new Promise((resolve, reject) => {

    const data = [

      {

        nama: "barang 1",

        harga: 2000,

      },

      {

        nama: "barang 2",

        harga: 3000,

      },

      {

        nama: "barang 3",

        harga: 4000,

      },

    ];

    console.log("Fetch data sedang dilakukan .....");

    setTimeout(() => {

      if (isSuccess) {

        resolve({ data });

      } else {

        reject("Data Gagal diambil");

      }

    }, 3000);

  });

};

fetchData(true)
  .then((result) => {
    console.log("Data berhasil diambil:", result.data);
  })
  .catch((error) => {
    console.log("Terjadi kesalahan:", error);
  });

// Coba copy paste codingan tersebut ke soal 9 dan soal 10. Perlu diperhatikan bahwa function promise tersebut memiliki sebuah parameter dimana jika dia bernilai true maka promise akan mengembalikan fulfilled. Sementara jika kamu memasukkan parameter bernilai false maka dia akan bernilai rejected. Pada promise tersebut juga terdapat waktu delay 3 detik sehingga kamu akan merasa seperti mengambil data layaknya dari server langsung. Untuk soal 9 cobalah jalankan function tersebut menggunakan .then() dan .catch().