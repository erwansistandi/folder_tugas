// 4. Destructure
// Terdapat sebuah object seperti berikut:

const obj = {

  title: "Massive Storm",

  content: "Massive storm destroy a Village",

  date: "22-10-21",

};

// Lakukan destructure pada setiap key di atas, setelah itu tampilkan melalui console.log masing-masing dari keynya.
const { title, content, date } = obj;

console.log(title);
console.log(content);
console.log(date);