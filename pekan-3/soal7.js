// 7. Array Filter
// Terdapat sebuah Array of Object seperti berikut:

const arrObj = [

  {

    name: "Bakso",

    price: 15000,

    rating: 7,

  },

  {

    name: "Mie Ayam",

    price: 18000,

    rating: 8.5,

  },

  {

    name: "Bubur Ayam",

    price: 10000,

    rating: 6,

  },

  {

    name: "Ketoprak",

    price: 12000,

    rating: 5,

  },

  {

    name: "Nasi Padang",

    price: 20000,

    rating: 9,

  },

];


// Cobalah terapkan array filter ke sebuah variabel baru dengan kondisi array yang didapatkan berisi object yang mempunyai rating lebih dari 7


  
  const highRatedItems = arrObj.filter((item) => item.rating > 7);
  
  console.log(highRatedItems);