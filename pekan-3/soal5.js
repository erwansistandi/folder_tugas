// 5. Array Map
// Terdapat sebuah array of Object seperti berikut:

const arrObj = [

  {

    name: "Headset",

    price: 250000,

  },

  {

    name: "Handphone",

    price: 150000,

  },

  {

    name: "Laptop",

    price: 850000,

  },

];



//  Buatlah sebuah array map ya nantinya akan mengembalikan kumpulan dari price yang tersedia pada arrObj tersebut. Setelah itu coba setor value baru tersebut ke sebuah variabel. Berikut adalah hasilnya:

const prices = arrObj.map((obj) => obj.price);
console.log(prices);