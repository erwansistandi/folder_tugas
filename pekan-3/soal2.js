// 2. String Literal
// Terdapat sebuah object seperti berikut:

 var obj = {

  name: "Erwan",

  age: 19,

  address: "Jalan Raya",

};

// Gunakanlah isi dari object dan string literal untuk membuat kalimat seperti berikut:
var kalimat = `Hallo Nama Saya ${obj.name}, saya umur ${obj.age} dan saya tinggal di ${obj.address}.`;
console.log(kalimat);
