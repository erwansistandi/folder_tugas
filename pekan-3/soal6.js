// 6. Advanced Array Map
// Diketahui sebuah array sebagai berikut:

const arr = [5, 3, 4, 7, 6, 9, 2];

// Cobalah buat sebuah array map yang dimana dia mengembalikan array baru dengan kondisi jika index genap maka value dikali 2 sementara jika ganjil maka dikali 3

const newArr = arr.map((valeu, index) => {
    if (index % 2 === 0 ) {
        return valeu * 2;
    } else {
        return valeu * 3;
    }
});

console.log(newArr);