// 8. Rest Parameter
// Buatlah sebuah function bernama sum yang menerima sebuah rest parameter. Parameter tersebut nantinya akan kita coba kalkulasikan keseluruhannya, 
function sum(...numbers) {
    let total = 0;
    for (let number of numbers) {
      total += number;
    }
    return total;
  }
  
  // Contoh pemanggilan fungsi
  console.log(sum(1, 2, 3, 4, 5)); // Output: 15
  console.log(sum(10, 20, 30)); // Output: 60
  console.log(sum(2, 4, 6, 8, 10)); // Output: 30