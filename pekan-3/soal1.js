// Terdapat sebuah function sepertri berikut:
// function luasPersegi(sisi) {

//  return sisi * sisi;

//  }

// Ubahlah Function Tersebut menjadi sebuah arrow Function.

const luasPersegi = (sisi) => sisi * sisi;

