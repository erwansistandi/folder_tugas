// 4. Ternary Operator
// Terdapat sebuah variabel bernama score dan sebuah variabel bernama result. 
// Jika score bernilai lebih dari sama dengan 70 maka result akan bernilai “Kamu lulus” 
// sementara jika tidak maka result akan bernilai “Kamu tidak lulus”. 
// Setelah itu tampilkan nilai result. 
// Gunakanlah ternary operator dalam mengerjakan soal ini. 
// Berikut adalah contoh jila variabel score bernilai 80:

var score = 80;
var result = score >= 70 ? "Kamu lulus" : "Kamu tidak lulus";

console.log(result);