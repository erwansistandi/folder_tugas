// 9. Membuat sebuah Function
// Buatlah sebuah function bernama luasPersegiPanjang yang menerima 2 parameter yaitu panjang dan lebar. 
// Nantinya function tersebut akan mengembalikan nilai hasil perkalian dari panjang dan lebar. 
// Berikut adalah contoh ketika function dipanggil:


function luasPersegiPanjang(panjang, lebar) {
    var luas = panjang * lebar;
    return luas;
  }
  
  
  var hasil = luasPersegiPanjang(6, 8);
  console.log(hasil);