// 6. For Loop bilangan ganjil
// Buatlah sebuah for loop yang dimana for loop ini akan menampilkan bilangan ganjil dari angka 0 sampai kurang dari 15.
// Gunakanlah conditional if dan modulo dalam pengerjaan soal ini. Berikut adalah hasil akhirnya:
for (var i = 0; i < 15; i++) {
    if (i % 2 !== 0) {
      console.log(i);
    }
  }