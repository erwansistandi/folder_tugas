// 7. Mengakses value object
// Terdapat sebuah object seperti berikut:

// var obj = {

//      nama: "farhan",

//      alamat: "Jalan Raya",

//      umur: 15

// }

// Tampilkan value dari key nama dan umur pada terminal.
var obj = {
    nama: "Erwan",
    alamat: "Jalan-Jalan",
    umur: 15
  };
  
  console.log(obj.nama);
  console.log(obj.umur);