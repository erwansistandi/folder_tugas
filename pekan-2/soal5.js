// 5. For loop kelipatan 3
// Cobalah untuk melakukan for loop yang berguna untuk menampilkan angka dengan kelipatan 3 dimulai dari 0 sampai kurang dari 20. Berikut adalah contoh hasil akhirnya.
for (var i = 0; i < 20; i += 3) {
    console.log(i);
  }