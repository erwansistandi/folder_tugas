// 8. For Loop with Array
// Terdapat sebuah array seperti berikut:

// var arr = ["Halo", "nama", "saya", "Farhan"];

// Buatlah sebuah for loop yang berguna untuk menampilkan data pada terminal dari index Pertama hingga akhir. 
// Gunakan index pada looping untuk menampilkannya ya, bukan di console secara manual

var arr = ["Halo", "nama", "saya", "Erwan"]
for (var i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}