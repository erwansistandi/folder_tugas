// 10. Memanggil function pada function
// Terdapat sebuah function bernama kuadrat seperti berikut:

// function kuadrat(nilai) {

//   return nilai * nilai;

// }

// Lalu buatlah sebuah program penghitung index berat badan, nantinya function tersebut akan mengembalikan nilai hasil kalkulasi dari rumus index berat badan, berikut adalah rumusnya:

// berat / (tinggi * tinggi)

// Gunakanlah function kuadrat yang telah disediakan dalam penerapan perkalian tinggi * tinggi.

function kuadrat(nilai) {
    return nilai * nilai;
  }
  
  function indexBeratBadan(berat, tinggi) {
    var tinggiKuadrat = kuadrat(tinggi);
    var index = berat / tinggiKuadrat;
    return index;
  }
  
  
  var beratBadan = 70;
  var tinggiBadan = 1.75;
  var hasilIndex = indexBeratBadan(beratBadan, tinggiBadan);
  console.log("Indeks Berat Badan: " + hasilIndex);