// 3. Penggunaan conditional if else
// Kita memiliki sebuah sebuah variabel bernama nilai, namun terdapat sebuah kondisi setelahnya dimana:

// Jika nilai lebih dari 80 maka di terminal akan menampilkan nilai A
// Jika nilai lebih dari 70 dan kurang dari sama dengan 80 maka di terminal akan menampilkan nilai B
// Jika nilai lebih dari 60 dan kurang dari sama dengan 70 maka di terminal akan menampilkan nilai C
// Jika nilai lebih dari 50 dan kurang dari sama dengan 60 maka di terminal akan menampilkan nilai D
// Jika nilai kurang dari atau sama dengan 50 maka di terminal akan menampilkan nilai E
// Berikut adalah contoh hasil jika variabel nilai memilki value 75:
var nilai = 75;

if (nilai > 80) {
  console.log("Nilai A");
} else if (nilai > 70 && nilai <= 80) {
  console.log("Nilai B");
} else if (nilai > 60 && nilai <= 70) {
  console.log("Nilai C");
} else if (nilai > 50 && nilai <= 60) {
  console.log("Nilai D");
} else {
  console.log("Nilai E");
}